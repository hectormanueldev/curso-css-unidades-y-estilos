# Curso de CSS Unidades y Estilos

## Temario

1. :heavy_plus_sign: Colores por nombre
1. :heavy_plus_sign: Colores hexadecimales
1. :heavy_plus_sign: Colores rgb
1. :heavy_plus_sign: Colores hsl
1. :heavy_plus_sign: Colores transparent y currentColor
1. :heavy_plus_sign: Propiedad opacity
1. :heavy_plus_sign: Pixeles
1. :heavy_plus_sign: Ems
1. :heavy_plus_sign: Rems
1. :heavy_plus_sign: Exs
1. :heavy_plus_sign: Chs
1. :heavy_plus_sign: Porcentajes
1. :heavy_plus_sign: Unidades del viewport
1. :heavy_plus_sign: Custom properties y función var
1. :heavy_plus_sign: Función url
1. :heavy_plus_sign: Función calc
1. :heavy_plus_sign: Función min y max
1. :heavy_plus_sign: Función clamp
1. :heavy_plus_sign: Estilos de fuentes
1. :heavy_plus_sign: Fuentes externas
1. :heavy_plus_sign: Fuentes de google
1. :heavy_plus_sign: Estilos de texto
1. :heavy_plus_sign: Iconos tipográficos
1. :heavy_plus_sign: Propiedad border-radius
1. :heavy_plus_sign: Propiedad outline
1. :heavy_plus_sign: Propiedad background-color
1. :heavy_plus_sign: Propiedad background-image
1. :heavy_plus_sign: Fondos múltiples
1. :heavy_plus_sign: Estilos de imágenes
1. :heavy_plus_sign: Estilos de listas
1. :heavy_plus_sign: Texto en columnas
1. :heavy_plus_sign: Estilos de tablas
1. :heavy_plus_sign: Estilos de formulario
